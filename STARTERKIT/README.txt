BUILD A THEME WITH Bootstrap Grid Only
---------------------------------------------------

The base Bootstrap Grid Only theme is designed to be easily extended by its
sub-themes. The sub-theme uses base theme functionality. You shouldn't modify
files of base theme.

---------------------------------------------------

1.  Setup the location for your new sub-theme.

    Copy the STARTERKIT folder out of the bootstrap_grid_only/ folder and rename
    it to be your new sub-theme.
    IMPORTANT: The name of your sub-theme must start with an alphabetic
    character and can only contain lowercase letters, numbers and underscores.

    For example: copy the themes/bootstrap_grid_only/STARTERKIT folder and
    rename it as themes/foo.

2.  Setup the basic information for your sub-theme.

    In your new sub-theme folder, rename the STARTERKIT.info.yml file to include
    the name of your new sub-theme.

    For example, rename the foo/STARTERKIT.info.yml file to foo/foo.info.yml.
    Edit the foo.info.yml file and change "name: 'STARTERKIT'" to "name: 'Foo'"
    and
    "libraries:
      - STARTERKIT/global-styling"

    to

    "libraries:
      - foo/global-styling"

    Then rename STARTERKIT.libraries.yml and STARTERKIT.theme files to include
    the name of your new sub-theme.

    For example, rename STARTERKIT.libraries.yml and STARTERKIT.theme to
    foo.libraries.yml and foo.theme. Edit the foo.theme file in your sub-theme's
    folder; replace ALL occurrences of "STARTERKIT" with the name of your
    sub-theme.

3.  Set your website's default theme.

    Log in as an administrator on your Drupal site, go to the Appearance page at
    admin/appearance and click the "Enable and set default" link next to your
    new sub-theme.

Optional steps:
---------------------------------------------------

4.	Modify the markup in Bootstrap Grid Only core's template files.

    If you want to modify any of the .html.twig template files in the Bootstrap
    Grid Only folder, copy them to your sub-theme's folder before making any
    changes. And then rebuild the theme registry.

    For example, copy bootstrap_grid_only/templates/page.html.twig to
    foo/templates/page.html.twig

5.  I recommend to use sass for writing css styles. For this in the sub-theme's
    folder you can use any .scss files located in style/scss. For compilation
    sass files i recommend to use compass. For it in root sub-theme's folder is
    located config.rb file with preinstalled settings. You can easy modify
    config.rb file if is required. Compiled files is located at styles/css and
    they include by default. I don't recommend modify css files if you use sass.

6.	Further extend your sub-theme.

    Discover further ways to extend your sub-theme by reading Drupal Theme Guide
    online at: https://drupal.org/theme-guide

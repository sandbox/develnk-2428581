ABOUT THEME
---------------------------------------------------
This theme designed for those who need only bootstrap's grid-system, on basic it
you can make own themes. Theme is not uses additional styles and js plugins from
standard bootstrap. On base of this theme you can get responsive behaviour, that
uses bootstrap classes from grid-system. Theme includes basic templates with
markup. Examples: page.html.twig and html.html.twig

WHAT ARE BASE THEMES, SUB-THEMES AND STARTER THEMES?
----------------------------------------------------
Often the best way to learn a system is to take an existing example and modify
it to see how it works. One big disadvantage of this learning method is that if
you break something and the original example worked before you hacked it,
there's very little incentive for others to help you.

Drupal's theming system has a solution to this problem: parent themes and
sub-themes. A "sub-theme" will inherit all its HTML markup, CSS, and PHP code
from its "parent theme" (also called a "base theme".) And with Drupal themes,
it's easy for a sub-theme to override just the parts of the parent theme it
wants to modify.

A "starter theme" is a sub-theme designed specifically to be a good starting
point for developing a custom theme for your website. It is usually paired with
a base theme.

So how do you create a theme with bootstrap_grid_only?
Theme includes bootstrap_grid_only base theme as well as a starter theme called
"STARTERKIT". You shouldn't modify any of the sass/css, .html.twig or php files
in bootstrap_grid_only/ folder. Instead you should create a sub-theme of
bootstrap_grid_only and put it in a folder outside of the root
bootstrap_grid_only/ folder.

SUGGESTED READING
----------------------------------------------------
Installation
	If you don't know how install a Drupal theme, there is a quick primer below in
	this document.

Building a theme bootstrap_grid_only
	See the STARTERKIT/README.txt file for full instructions.

Theme .info.yml
	Your sub-theme's .info.yml file holds the  basic inforation about your theme:
	its name, description, features, template regions, CSS files, and JavaScript.

CSS
	Once you have created your sub-theme, look at the README.txt in your
	sub-theme's "styles" folder.

Templates
	Now take a look at the README.txt in your sub-theme's templates folder.

INSTALLATION
----------------------------------------------------
1.	Download theme.

2.	Unpack the downloaded file, place it in your Drupal installation under
    themes/.
    Additional installation folders can be used; see
    https://drupal.org/getting-started/install-contrib/themes for more
    information.

3.  Log in as an administrator on your Drupal site and go to the Appearance page
    at admin/appearance. You will see the bootstrap_grid_only theme listed under
    the Disabled Themes heading with links on how to create your own sub-theme.
    You can optionally make bootstrap_grid_only the default theme.

4.  Now build your own sub-theme by reading the STARTERKIT/README.txt file.
